# Personified

## Table of contents

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers

## Introduction

Provides configurable blocks to render JSON results.

## Requirements

This module requires the following modules:
- [JSON Template](https://www.drupal.org/project/json_template)

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Enable the module at Administration > Extend
2. Place the Personified Data or Personified Message block from the block placement page.

## Maintainers

This module is maintained by developers at Morpht. For more information on the
company and our offerings, see [morpht.com](https://morpht.com/).
