/**
 * @file
 * Initializes all Personified Message blocks.
 */

(function ($, Drupal, cookies) {
  Drupal.behaviors.personifiedMessage = {
    attach: function attach(context, settings) {
      function getQueryParams() {
        let result = {};
        let params = window.location.search.substring(1).split('&');
        for (let i = 0; i < params.length; i++) {
          let param = params[i].split('=');
          result[param[0]] = (param[1] === undefined) ? true : decodeURIComponent(param[1]);
        }
        return result;
      }
      function isJsonString (str) {
        try {
          JSON.parse(str);
        }
        catch (e) {
          return false;
        }
        return true;
      }
      if (typeof settings.personifiedMessage === 'undefined') {
        return;
      }
      let localStorageFormatted = {}
      for (const [key, value] of Object.entries(localStorage)) {
        if (isJsonString(value)) {
          localStorageFormatted[key] = JSON.parse(value)
        }
        else {
          localStorageFormatted[key] = value
        }
      }
      let data = {
        query: getQueryParams(),
        cookie: cookies.get(),
        localstorage: localStorageFormatted,
      };
      $.each(settings.personifiedMessage, function (id, args) {
        let element = $('#' + id, context);
        if (element.length) {
          element.html(Drupal.jsonTemplate.plugins[args.transformer](args.template, data));
        }
      });
    },
  };
})(jQuery, Drupal, window.Cookies);
