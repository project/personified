<?php

namespace Drupal\personified\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\json_template\Plugin\JsonTransformerManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'personified_text' formatter.
 *
 * @FieldFormatter(
 *   id = "personified_text",
 *   label = @Translation("Personified"),
 *   field_types = {
 *     "string",
 *     "string_long",
 *     "text",
 *     "text_long",
 *     "text_with_summary"
 *   }
 * )
 */
class PersonifiedTextFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The JSON transformer manager.
   *
   * @var \Drupal\json_template\Plugin\JsonTransformerManagerInterface
   */
  protected $jsonTransformer;

  /**
   * PersonifiedTextFormatter constructor.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\json_template\Plugin\JsonTransformerManagerInterface $json_transformer
   *   The JSON transformer manager.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    JsonTransformerManagerInterface $json_transformer,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->jsonTransformer = $json_transformer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('plugin.manager.json_template.transformer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'transformer' => 'handlebars',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    // Get all defined transformers.
    $transformer_options = [];
    foreach ($this->jsonTransformer->getDefinitions() as $transformer) {
      $transformer_options[$transformer['id']] = $transformer['title'];
    }
    $element['transformer'] = [
      '#type' => 'select',
      '#title' => $this->t('Transformer'),
      '#description' => $this->t('The transformer to use to display the results.'),
      '#options' => $transformer_options,
      '#default_value' => $this->getSetting('transformer'),
      '#required' => TRUE,
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Personified: @transformer', ['@transformer' => $this->getSetting('transformer')]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $definition = $this->jsonTransformer->getDefinition($this->getSetting('transformer'));
      $elements[$delta] = [
        '#theme' => 'personified_message',
        '#template' => $item->value,
        '#transformer' => $this->getSetting('transformer'),
        '#attached' => ['library' => [$definition['library']]],
      ];
    }

    return $elements;
  }

}
